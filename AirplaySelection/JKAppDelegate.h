//
//  JKAppDelegate.h
//  AirplaySelection
//
//  Created by Joris Kluivers on 7/11/12.
//  Copyright (c) 2012 Joris Kluivers. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface JKAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSButton *toggleButton;

/*!
 * Array of all available airplay sources available for playback.
 */
@property (readonly) NSArray *airplaySources;

- (IBAction) toggle:(id)sender;
- (IBAction) outputSourceSelectionChange:(id)sender;
- (IBAction) selectAllSources:(id)sender;

@end
